FROM node:10-alpine


ENV application_directory=/usr/src/app

# args from env
ARG UNK_ANA_REDIS_URI
ARG UNK_ANA_WEB_HOOK_SCREENSHOT_URL
ARG UNK_ANA_ENVIRONMENT
ARG UNK_ANA_SCREENSHOT_SECRET_KEY
ARG PORT


ENV UNK_ANA_DOCKER=true
ENV UNK_ANA_MEDIA_STORAGE="/data"


RUN mkdir -p $application_directory

# https://github.com/puppeteer/puppeteer/issues/4824
# Installs latest Chromium (73) package.
RUN apk update && apk upgrade && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories && \
    apk add --no-cache \
        chromium \
        harfbuzz \
        "freetype>2.8" \
        ttf-freefont \
        nss

# If running Docker >= 1.13.0 use docker run's --init arg to reap zombie processes, otherwise
# uncomment the following lines to have `dumb-init` as PID 1
# ADD https://github.com/Yelp/dumb-init/releases/download/v1.2.0/dumb-init_1.2.0_amd64 /usr/local/bin/dumb-init
# RUN chmod +x /usr/local/bin/dumb-init
# ENTRYPOINT ["dumb-init", "--"]

# Tell Puppeteer to skip installing Chrome. We'll be using the installed package.
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true



# Add user so we don't need --no-sandbox.
RUN addgroup -S pptruser && adduser  -S -g pptruser pptruser \
    && mkdir -p /home/pptruser/Downloads   \
    && chown -R pptruser:pptruser /home/pptruser \
    && chown -R pptruser:pptruser $application_directory

# Run everything after as non-privileged user.
USER pptruser

# Create app directory

RUN npm cache clear --force
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY routes ./$application_directory/routes
COPY bin ./$application_directory/bin
COPY public ./$application_directory/public
COPY views ./$application_directory/views
COPY constants ./$application_directory/constants
COPY controllers ./$application_directory/controllers

#
COPY app.js ./$application_directory/


## run seeders

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package.json ./$application_directory/


WORKDIR ./$application_directory

RUN npm install --only=production


EXPOSE 3000
# redis
EXPOSE 6379
#CMD [ "sh", "-c", "ls -a /backend"]
# Run app
ENTRYPOINT npm start

Screenshot and heatmap module  for [unknownanalytics](unknownanalytics.com/).

In red square : 

![module](docs/arch.png)

This is the module that takes screenshots and plots heatmaps (click and mouse movement ) of a web page  for [unknownanalytics](unknownanalytics.com/)

The app connects to a redis server and subscribe to channels to handle events sent by the [hub](https://gitlab.com/unknown-inc/hub) module.  
The app supports the http calls, [details](/routes/screens.js) . 


For each page view, this app will create a screen view and a thumbnail. [details](/controllers/screen-shot.js)

### Features


##### heatmap (experimental)

Instead of generating the heatmap on the client side, the app will pre-render a heatmap mask as png for a given page under the express route `/screen?width=&height=&data=`, where `data` are the events stored since last track (click, mousemove). 
By this approach we prepare the heatmaps masks for each page (transparent png) for admin dashboard without the need to re-render them in the client side (data loading + drawing huge records ..). Also, we can display  multiple heatmaps in the dashboard without impacting the client resources. 

##### Screenshot ([under the hood](https://github.com/puppeteer/puppeteer/))

For each page of the client website, the app generates screenshots for some default sizes . [details](/constants/screen.js)
This is may be not very precise (as page rendering depends on the client browser rendering and windows sizes ...,), but our goal is to provide a very closed view without the need to transfer the user screen.

### All pages must be public
We do not transfer HTML from the client side, instead, given the url of the page, this app will try to get the same url. So, if any page requires some kind of authorisation, the app will not take the exact screen shot.

### Env variables 

```
UNK_ANA_REDIS_URI
UNK_ANA_ENVIRONMENT
PORT
UNK_ANA_WEB_HOOK_SCREENSHOT_URL
UNK_ANA_SCREENSHOT_SECRET_KEY
```

Note that we are using `host.docker.internal` as docker host because this is the local address of docker in windows._

#### Deployment 

1- build 

`docker build  -t unk-node-screen-shot .`

2 - Run 

`docker run -d -t -i \
    -p 5001:5000 \
    -e UNK_ANA_REDIS_URI=host.docker.internal  \
    -e UNK_ANA_SCREENSHOT_SECRET_KEY=secret_12345_change_me  \
    -e UNK_ANA_ENVIRONMENT=staging  \
    -e UNK_ANA_WEB_HOOK_SCREENSHOT_URL=host.docker.internal:3001  \
    -e PORT=5000  \
    -v C:\\www\\unknown:/data \
     unk-node-screen-shot`

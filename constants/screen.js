const SCREEN = {
    // iphone 6
    'iphone_6': {
        viewport: {
            w: 375,
            h: 667,
            dpr: 2,
            isMobile: true,
            'hasTouch': true,
            'isLandscape': false
        },
        ua: 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1'
    },
    // ipad
    'ipad': {
        viewport: {
            w: 768,
            h: 1024,
            dpr: 2,
            isMobile: true,
            'hasTouch': true,
            'isLandscape': false
        },
        ua: 'Mozilla/5.0 (iPad; CPU OS 11_0 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Version/11.0 Mobile/15A5341f Safari/604.1',
    }, // ipad Pro
    'ipad_pro': {
        viewport: {
            w: 1024,
            h: 1366,
            dpr: 2,
            isMobile: true
        },
        ua: 'Mozilla/5.0 (iPad; CPU OS 11_0 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Version/11.0 Mobile/15A5341f Safari/604.1'
    },
    desktop_1: {
        viewport: {
            w: 1024,
            h: 768
        },
    },
    desktop_2: {
        viewport: {
            w: 1280,
            h: 720
        },
    },
    desktop_3: {
        viewport: {
            w: 1920,
            h: 1080
        },
    }

};

module.exports = {
    SCREEN_RESOLUTIONS: SCREEN
};

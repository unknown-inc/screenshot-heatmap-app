const screenConstants = require('../constants/screen');
const path = require('path');
const puppeteer = require('puppeteer-extra');
const Queue = require('bull');
const Redis = require("ioredis");
const logger = require('morgan');
const http = require('http');
const fs = require('fs');
const sharp = require('sharp');

// Add stealth plugin and use defaults (all tricks to hide puppeteer usage)
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin({}));


/*
 // Stealth plugins are just regular `puppeteer-extra` plugins and can be added as such
 * const UserAgentOverride = require("puppeteer-extra-plugin-stealth/evasions/user-agent-override")
 * // Define custom UA, locale and platform
 * const ua = UserAgentOverride({ userAgent: "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)", locale: "de-DE,de;q=0.9", platform: "Win32" })
 * puppeteer.use(ua)
 *
 */

const inDockerEnv = process.env.UNK_ANA_DOCKER;
const HTTP_PREFIX = 'https://';
let mediaFolder = process.env.UNK_ANA_MEDIA_STORAGE || '';
let redisHost = process.env.UNK_ANA_REDIS_URI || "localhost";
let queueName = 'screen_shot' + '_' + process.env.UNK_ANA_ENVIRONMENT;
let SCREEN_SHOT_DONE_REDIS_CHANNEL = process.env.UNK_ANA_SCREENSHOT_REDIS_CHANNEL_FINISH || "UNK_ANA_SCREENSHOT_REDIS_CHANNEL_FINISH";
let UNK_ANA_WEB_HOOK_SCREENSHOT_URL = process.env.UNK_ANA_WEB_HOOK_SCREENSHOT_URL || "http://localhost:3002/hooks/screenshot";
let app = {};


const redisPub = new Redis({host: redisHost});
const screeShotQueue = new Queue(queueName, {redis: {host: redisHost}});

console.time('Redis');
redisPub.on('error', function (e) {
  console.log(e)
});
redisPub.on('connect', function (e) {
  // continue program logic
  console.timeEnd('Redis')
});

screeShotQueue.on('error', function (err) {
  console.log('Oops... ', err);
});

screeShotQueue.process(function (job, done) {
  (async () => {
    process.hrtime();
    if (!(app.page || app.browser)) {
      await initBrowser();
    }
    const data = job.data;
    let pageUrl = data.page_url;
    let pageId = data.page_id;
    let pageResolution = data.resolution;
    const resolution = screenConstants.SCREEN_RESOLUTIONS[pageResolution];
    let pathToScreenShot = null;
    if (pageUrl && pageId) {
      // TODO, https
      /*if (url.substr(0, HTTP_PREFIX.length) !== HTTP_PREFIX) {
          url = HTTP_PREFIX + url;
      }*/
      try {
        pathToScreenShot = await makeScreenShotForSingleResolution(pageId, pageUrl, pageResolution);
        console.log("pathToScreenShot >>", pathToScreenShot);
      } catch (err) {
        console.log(err);
        return done(err);
      }
      if (pathToScreenShot) {

        const hrend = process.hrtime();
        console.info('Execution time (hr): %ds %dms', hrend[0], hrend[1] / 1000000);
        console.log('sending to redis');
        let dataToServer = JSON.parse(JSON.stringify(data));
        const pathToThumb = path.join(mediaFolder, _getPicturePathName(['thumb', pageId, resolution.viewport.w, resolution.viewport.h, Date.now()]));
        console.log("pathToThumb >>", pathToThumb);
        sharp(pathToScreenShot)
            .resize({
              width: 100,
              kernel: sharp.kernel.nearest,
              fit: 'contain',
              background: {r: 255, g: 255, b: 255, alpha: 0.5}
            })
            .toFile(pathToThumb)
            .then(() => {
              dataToServer['app_token'] = process.env.UNK_ANA_SCREENSHOT_SECRET_KEY;
              dataToServer['thumbnail_path'] = pathToThumb;
              dataToServer['screenshot_path'] = pathToScreenShot;
              // SEND WEB HOOKS
              sendHook(dataToServer);
              // FUTURE USAGE
              redisPub.publish(SCREEN_SHOT_DONE_REDIS_CHANNEL, JSON.stringify(Object.assign({}, data, {path: pathToScreenShot})));
              return done();
            });
      } else {
        console.log('Path is null, please verify , !!!!')
        return done("empty data");
      }

    } else {
      console.log("empty data");
      done("No url");
    }
  })();
});


function _getPicturePathName(chunks) {
  return chunks.join('_') + '.png';
}

/**
 *
 * @param page_id
 * @param page_url
 * @param resolution_key
 * @param options
 * @returns {Promise<string>}
 */
async function makeScreenShotForSingleResolution(page_id, page_url, resolution_key, options) {
  if (page_url &&
      (
          (page_url.indexOf(HTTP_PREFIX) === 0) ||
          (page_url.indexOf("http://localhost") === 0) ||
          (page_url.indexOf("https://localhost") === 0) ||
          (page_url.indexOf("http://host.docker.internal") === 0)
      )) {
    console.log(page_url);
    let page = app.page;

    const resolution = screenConstants.SCREEN_RESOLUTIONS[resolution_key];
    let viewport = resolution.viewport;
    console.log(viewport);
    // TODO, verify all params
    let pathToFile = path.join(mediaFolder, _getPicturePathName([page_id, viewport.w, viewport.h, Date.now()]));

    if (!fs.existsSync(pathToFile)) {
      let _options = {
        width: viewport.w,
        height: viewport.h
      };
      // check for dpr
      if (viewport.dpr) {
        _options.deviceScaleFactor = viewport.dpr
      }
      // check for dpr
      if (viewport.isMobile) {
        _options.isMobile = viewport.isMobile
      }
      await page.setViewport(_options);

      console.log(_options);
      if (resolution.ua) {
        console.log("resolution.ua");
        console.log(resolution.ua);
        await page.setUserAgent(resolution.ua);
      } else {
        console.log("default user agent ");
        //console.log(originalUserAgent);
        //await page.setUserAgent(originalUserAgent);
      }
      //
      await page.goto(page_url, {waitUntil: 'load', timeout: 120000});

      //
      let dimensions = await page.evaluate(
          () => [document.getElementsByTagName('html')[0].offsetWidth, document.getElementsByTagName('html')[0].offsetHeight]
      );
      console.log("dimensions", dimensions);
      let screenShotOptions = {
        path: pathToFile,
        //fullPage: true,
        //height: pageHeight + 'px'
        // quality: 100
        clip: {x: 0, y: 0, width: dimensions[0], height: dimensions[1]}
      };
      /* for transparent
      if (options && options.transparent) {
          await page.evaluate(() => document.body.style.background = 'transparent');
      }*/
      console.log(Object.assign({}, screenShotOptions, options || {}))
      await page.screenshot(Object.assign({}, screenShotOptions, options || {}));
      console.log("#### pathToFile ####", pathToFile);
      return pathToFile;
    }
    return null;
  }
}

/**
 *
 * @param page_id
 * @param page_url
 * @param resolution
 * @param options
 */
function addToQueue(page_id, page_url, resolution, options) {
  screeShotQueue.add({
    page_url: page_url,
    page_id: page_id,
    resolution: resolution,
    options: options
  });
}

/**
 * Init the browser
 * @returns {Promise<void>}
 */
async function initBrowser() {
  const browser = inDockerEnv ? await puppeteer.launch({
    executablePath: '/usr/bin/chromium-browser',
    args: ['--disable-dev-shm-usage', '--no-sandbox']
  }) : await puppeteer.launch({headless: true});

  app.browser = browser;
  app.page = await browser.newPage();
  /* originalUserAgent = await browser.userAgent();

   await app.page.setExtraHTTPHeaders({
     'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8'
   });*/
}

/**
 * Make all screen shots
 * @param page_id
 * @param page_url
 * @param options
 */
const makeScreenShots = function (page_id, page_url, options) {
  console.log('#### start making ');
  Object.keys(screenConstants.SCREEN_RESOLUTIONS).forEach(resolution => {
    addToQueue(page_id, page_url, resolution, options);
  })
};


/**
 * Send screen shot hooks
 * @param data
 */
function sendHook(data) {
  console.log(data);
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'//,
      // https://stackoverflow.com/questions/34547954/nodejs-typeerror-buffer-is-not-a-function-but-its-a-function
      //'Content-Length': Buffer.Buffer.byteLength(data)
    }
  };
  const req = http.request(UNK_ANA_WEB_HOOK_SCREENSHOT_URL, options, (res) => {
    console.log(`statusCode: ${res.statusCode}`);
    res.on('data', (d) => {
      // process.stdout.write(d)
    })
  });
  req.on('error', (error) => {
    console.log("Error");
    console.error(error)
  });
  req.end(JSON.stringify(data));
}

/**
 * Ensure to launch puppeteer in async mode
 */
initBrowser();

module.exports = {
  makeScreenShots: makeScreenShots,
  makeScreenShotForSingleResolution: makeScreenShotForSingleResolution
};
